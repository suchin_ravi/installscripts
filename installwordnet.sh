sudo apt-get install pkg-config tcl8.4-dev tk8.4-dev
wget http://wordnetcode.princeton.edu/3.0/WordNet-3.0.tar.gz
tar -xvzf WordNet-3.0.tar.gz
cd WordNet-3.0/
./configure --with-tcl=/usr/lib/tcl8.4/ --with-tk=/usr/lib/tk8.4/
make
sudo make install
echo "export PATH=$PATH:/usr/local/WordNet-3.0/bin">>~/.profile
echo "Install Completete"

